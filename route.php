<?php
    require_once "Controller/LibraryController.php";
    require_once "Controller/LoginController.php";
    
    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');
    
    
    if (!empty($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = 'publicHome';
    }

    $params = explode('/', $action);

    $libraryController = new LibraryController();

    switch ($params[0]) {
        case 'publicHome': 
            $libraryController->showPublic(); 
            break;
        case 'publicList':
            $libraryController->showLibrary();
            break;
        case 'privateList':
            $libraryController->showPrivate();
            break;
        default: 
            echo('404 Page not found'); 
            break;
    }


