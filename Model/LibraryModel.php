<?php

class LibraryModel{

    private $db;
    function __construct(){
         $this->db = new PDO('mysql:host=localhost;'.'dbname=db_biblioteca;charset=utf8', 'root', '');
    }

    function getBook(){
        $opcion = $this->db->prepare( "select * from libros");
        $opcion->execute();
        $libro = $opcion->fetchAll(PDO::FETCH_OBJ);
        return $libro;
    } 
    function insertBook($titulo, $genero, $descripcion, $autor_id){
        $sentencia = $this->db->prepare("INSERT INTO libros(titulo, genero , descripcion, autor_id) VALUES(?, ?, ?, ?)");
        $sentencia->execute(array($titulo,$genero,$descripcion, $autor_id ));
    }


}