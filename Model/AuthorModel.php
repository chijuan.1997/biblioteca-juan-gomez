<?php
    class AuthorModel{
        private $db;
        function __construct(){
            $this->db = new PDO('mysql:host=localhost;'.'dbname=db_biblioteca;charset=utf8', 'root', '');
        }
        function getAuthor(){
            $opcion = $this->db->prepare( "select * from autor");
            $opcion->execute();
            $autor = $opcion->fetchAll(PDO::FETCH_OBJ);
            return $autor;
        }
        function insertAuthor($id_autor, $nombre, $anio){
            $sentencia = $this->db->prepare("INSERT INTO autor(id_autor, nombre , anio) VALUES(?, ?, ?)");
            $sentencia->execute(array($id_autor, $nombre, $anio));
        }
    }