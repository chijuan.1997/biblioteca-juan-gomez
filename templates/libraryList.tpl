{include file='templates/header.tpl'}
{include file='templates/navPublic.tpl'}
<div>
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
          <div class="card shadow-2-strong">
            <div class="card-body p-5 text-center">
              <h1>Libros Disponibles</h1>
              <table class="table">
              <thead>
                <tr>
                    <th scope="col">titulo</th>
                    <th scope="col">genero</th>
                    <th scope="col">descripcion</th>
                    <th scope="col">#autor</th>
                </tr>
              </thead>
              <tbody>
                {foreach from=$libro item=$libros}

                  <tr>
                    <td>{$libros->titulo}</td>
                    <td>{$libros->genero}</td>
                    <td>{$libros->descripcion}</td>
                    <td>{$libros->autor_id}</td>
                  </tr>

                {/foreach}
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<div>
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
          <div class="card shadow-2-strong">
            <div class="card-body p-5 text-center">
              <h1>Autores y anio de lanzamiento</h1>
              <table class="table">
              <thead>
                <tr>
                    <th scope="col">id_autor</th>
                    <th scope="col">nombre</th>
                    <th scope="col">anio</th>
                </tr>
              </thead>
              <tbody>
                {foreach from=$autor item=$autores}

                  <tr>
                    <td>{$autores->id_autor}</td>
                    <td>{$autores->nombre}</td>
                    <td>{$autores->anio}</td>
                  </tr>

                {/foreach}
              </tbody>
            </table>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



{include file='templates/footer.tpl'}