<footer>
        <h4>Nuestras redes sociales</h4>
        <a href="https://twitter.com/home?lang=es" target="_blank"><img class="logo" src="images/logotwiter.png" alt="logotwiter"></a>
        <a href="https://www.instagram.com/"><img class="logo" src="images/logoinsta.png" alt="logoinstagram"></a>
        <a href="https://www.facebook.com/"><img class="logo" src="images/logofacebook.png" alt="logofacebook"></a>
        <h4>©Centinelas de las Sierras 2021 - Desarrollado por Juan Gomez </h4>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>