{include file='templates/header.tpl'}

<section class="vh-100" style="background-color: #FF7400;">
        <div class="container py-5 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
              <div class="card shadow-2-strong">
                <div class="card-body p-5 text-center">
      
                  <h3 class="mb-5">Bienvenido</h3>
                  <form class="form-alta" action="verify" method="post">
                    <div class="form-outline mb-4">
                        <input type="email" id="typeEmailX-2" class="form-control form-control-lg" name="usuario"/>
                        <label class="form-label" >Usuario</label>
                    </div>
        
                    <div class="form-outline mb-4">
                        <input type="password" id="typePasswordX-2" class="form-control form-control-lg" name="contrasenia"/>
                        <label class="form-label" >Contrasenia</label>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
                    <button class="btn btn-primary btn-lg btn-block" href="sign in/">Registrarse</button>
                    <button class="btn btn-primary btn-lg btn-block" href="publicHome/">Modo publico</button>
                  </form>
      
                </div>
              </div>
            </div>
          </div>
        </div>
</section>

{include file='templates/footer.tpl'}