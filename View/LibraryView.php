<?php
    require_once './libs/smarty-3.1.39/libs/Smarty.class.php';

    class LibraryView {
        private $smarty;

        function __construct() {
            $this->smarty = new Smarty();
        }

        function showPublicHome(){
            $this->smarty->display('templates/publicHome.tpl');
        }
        function showLibraryPublic($libro,$autor){
            $this->smarty->assign('titulo', 'Lista de Libros');        
            $this->smarty->assign('libro', $libro);
            $this->smarty->assign('autor',$autor);
            $this->smarty->display('templates/libraryList.tpl');
        }
        function showPrivateHome(){
            $this->smarty->display('templates/privateHome.tpl');
        }

    }