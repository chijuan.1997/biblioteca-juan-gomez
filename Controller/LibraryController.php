<?php
    require_once "./Model/LibraryModel.php";
    require_once "./View/LibraryView.php";
    require_once "./Model/AuthorModel.php";

    class LibraryController{
        private $model;
        private $view;
        private $authormodel;

        
    
        function __construct(){
            $this->model = new LibraryModel();
            $this->view = new LibraryView();
            $this->authormodel = new AuthorModel();
            $this->authHelper = new AuthHelper();
        }
    
        function showPublic(){
            $this->view->showPublicHome();
        }
        function showLibrary(){
            $libro = $this->model->getBook();
            $autor = $this->authormodel->getAuthor();
            $this->view->showLibraryPublic($libro,$autor);
        }   
        function showPrivate(){
            $this->view->showPrivateHome();
        }
            
                        
        
    }